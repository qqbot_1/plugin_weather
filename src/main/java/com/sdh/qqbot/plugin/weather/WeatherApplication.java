package com.sdh.qqbot.plugin.weather;

import com.gitee.starblues.bootstrap.SpringPluginBootstrap;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 这是一个示例插件示例项目
 */
@SpringBootApplication
@Slf4j
public class WeatherApplication extends SpringPluginBootstrap {
    public static void main(String[] args) {
        new WeatherApplication().run(args);
    }
}
