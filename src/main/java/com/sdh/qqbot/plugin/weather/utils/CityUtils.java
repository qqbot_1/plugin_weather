package com.sdh.qqbot.plugin.weather.utils;

import com.alibaba.fastjson2.JSON;
import com.sdh.qqbot.main.utils.okhttp.OkHttpUtil;
import com.sdh.qqbot.plugin.weather.config.Config;
import com.sdh.qqbot.plugin.weather.entity.WeatherCityEntity;
import com.sdh.qqbot.plugin.weather.entity.WeatherEntity;

public class CityUtils {
    /**
     * 通过城市字符串查询所在经纬度
     *
     * @param City 城市中文字符串
     * @return 城市实体对象
     */
    public static WeatherCityEntity queryCityDesByCityName(String City) {
        String AMAP_URL = Config.AMAP_URL;
        AMAP_URL += Config.AMAP_TOKEN;
        AMAP_URL += "&address=";
        AMAP_URL += City;
        String json = OkHttpUtil.get(AMAP_URL);
        return JSON.parseObject(json, WeatherCityEntity.class);
    }
    /**
     * 根据经纬度查询天气
     *
     * @param location 城市经纬度
     * @return 天气对象
     */
    public static WeatherEntity queryWeatherByLocation(String location) {
        String CY_URL = Config.CY_URL + Config.CY_TOKEN + "/";
        CY_URL += location;
        CY_URL += "/weather.json";
        String json = OkHttpUtil.get(CY_URL);
        return JSON.parseObject(json, WeatherEntity.class);
    }
}
