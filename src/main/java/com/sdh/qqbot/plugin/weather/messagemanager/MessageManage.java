package com.sdh.qqbot.plugin.weather.messagemanager;

import com.gitee.starblues.annotation.Extract;
import com.sdh.qqbot.main.entity.message.MessageEntity;
import com.sdh.qqbot.main.entity.message.SendMessageEntity;
import com.sdh.qqbot.main.message.ReceiverMessage;
import com.sdh.qqbot.main.websocker.WebSocketTool;
import com.sdh.qqbot.plugin.weather.entity.WeatherCityEntity;
import com.sdh.qqbot.plugin.weather.entity.WeatherEntity;
import com.sdh.qqbot.plugin.weather.utils.CityUtils;
import com.sdh.qqbot.plugin.weather.utils.formatWeatherInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * 请不要修改bus和order的值
 * 修改scene的值为自己插件的信息
 */
@Slf4j
@Component
@Extract(bus = "plugin", scene = "weather", order = 1)
public class MessageManage implements ReceiverMessage {
    /**
     * 消息管理器，接收消息并处理
     *
     * @param message 消息实体
     */
    @Override
    public void MessageManager(MessageEntity message) {
        String[] msgArr = message.getMessage().trim().split(" ");
        if("天气".equals(msgArr[0])) {
            Map<String, String> params = new HashMap<>(2);
            if ("private".equals(message.getMessageType())) {
                params.put("user_id", message.getSender().getUserId());
            } else {
                params.put("group_id", message.getGroupId());
            }
            String weatherMsg = null;
            if ("帮助".equals(msgArr[1])) {
                weatherMsg = help();
            }else{
                String city = msgArr[1];
                log.info("开始查询" + city + "的天气");
                //获取经纬度
                WeatherCityEntity weatherCityEntity = CityUtils.queryCityDesByCityName(city);
                log.info(weatherCityEntity.toString());
                //根据经纬度查询天气
                WeatherEntity weatherEntity = CityUtils.queryWeatherByLocation(weatherCityEntity.getGeocodes().get(0).getLocation());
                int dayCount = msgArr.length>2?Integer.parseInt(msgArr[2]):2;
                weatherMsg = formatWeatherInfo.format(weatherEntity, dayCount, weatherCityEntity.getGeocodes().get(0).getFormattedAddress());
            }
            params.put("message", weatherMsg);
            SendMessageEntity sendMessageEntity = SendMessageEntity.builder().action("send_msg").params(params)
                    .echo("weather").build();
            WebSocketTool.send(sendMessageEntity);
        }else{
            log.info(String.valueOf(message));
        }
    }
    public String help() {
        StringBuilder builder = new StringBuilder();
        builder.append("天气帮助：").append("\n");
        builder.append("天气 [地址]（越详细越好） [预报天数](可选，最多5天)");
        return builder.toString();
    }
}
