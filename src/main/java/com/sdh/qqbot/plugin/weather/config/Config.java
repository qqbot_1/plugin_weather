package com.sdh.qqbot.plugin.weather.config;

/**
 * 存放插件所需要的配置项，或者需求请求的Api地址及Token。
 *
 * @author SDH
 * @date 2022/10/19
 */
public class Config {
    public static final String AMAP_URL="https://restapi.amap.com/v3/geocode/geo?key=";
    //申请地址 https://lbs.amap.com/
    public static final String AMAP_TOKEN="";

    public static final String CY_URL = "https://api.caiyunapp.com/v2.5/";
    //申请地址 https://dashboard.cyapi.cn/user/sign_in/
    public static final String CY_TOKEN = "";
}
