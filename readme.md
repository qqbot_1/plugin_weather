# plugin_weather

#### 介绍

qqbot项目下天气插件<br>
主项目地址:https://gitee.com/SDH734/qqbot

#### 使用说明

聊天框中按照如下格式输入信息:<br>
    天气 [地址]（越详细越好） [预报天数]\(可选，最多5天\)<br>
例: 天气 北京<br>
    天气 北京 4<br>

#### 参与贡献

1.  提交代码
2.  新建 Pull Request
